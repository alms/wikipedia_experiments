package ru.spbu.apmath.pt.dump.util;

import com.google.common.collect.ImmutableMap;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.util.EnumSet;
import java.util.Set;


public class WikiArchiveProcessor {
    private static final Logger LOG = Logger.getLogger(WikiArchiveProcessor.class);

    private final File wikiArchiveFile;

    public WikiArchiveProcessor(final File wikiArchiveFile) {
        this.wikiArchiveFile = wikiArchiveFile;
    }

    public void process(final IWikiPageListener pageListener) throws Exception{
        final SAXParserFactory factory = SAXParserFactory.newInstance();
		final SAXParser parser = factory.newSAXParser();

        parser.parse(wikiArchiveFile, new PageExtractorSAXListener(pageListener));
    }

     private static class PageExtractorSAXListener extends DefaultHandler {
        private enum State {
            PAGE, REVISION, TEXT, TITLE, ID
        }

        private final static ImmutableMap<String, State> TAG_TO_STATE =
                ImmutableMap.of("page", State.PAGE,
                                "revision",  State.REVISION,
                                "text", State.TEXT,
                                "title", State.TITLE,
                                "id", State.ID
                );


        private final Set<State> trace;
        private final IWikiPageListener wikiPageListener;

        private final StringBuilder pageTitle;
        private final StringBuilder pageText;
        private final StringBuilder pageId;

        private boolean isInterestPage;

        private int articlesProcessed;

        private PageExtractorSAXListener(final IWikiPageListener wikiPageListener) {
            this.articlesProcessed = 0;

            this.trace = EnumSet.noneOf(State.class);
            this.wikiPageListener = wikiPageListener;

            this.pageText = new StringBuilder();
            this.pageTitle = new StringBuilder();
            this.pageId = new StringBuilder();
        }

        @Override
        public void startDocument() throws SAXException {
            LOG.info("start");
            try {
                wikiPageListener.start();
            } catch (Exception e) {
                throw new SAXException(e);
            }
        }

        @Override
        public void endDocument() throws SAXException {
            try {
                wikiPageListener.end();
            } catch (Exception e) {
                throw new SAXException(e);
            }
            LOG.info("finish");
        }

        @Override
        public void startElement(final String uri, final String localName, final String qName, final Attributes attributes) throws SAXException {
            final State cur = TAG_TO_STATE.get(qName);

            if (cur != null) {
                trace.add(cur);
            }

            if (cur == State.PAGE) {
                pageTitle.setLength(0);
                pageText.setLength(0);
                pageId.setLength(0);

                if (++articlesProcessed % 100000 == 0) {
                    LOG.info("articles processed " + articlesProcessed);
                }
            } else if (cur == State.TEXT) {
                ParsingUtils.normalizeWikiPageName(pageTitle);
                try {
                    isInterestPage = wikiPageListener.isInterestPage(pageId, pageTitle);
                } catch (Exception e) {
                    throw new SAXException(e);
                }
            }

        }

        @Override
        public void endElement(final String uri, final String localName, final String qName) throws SAXException {
            final State cur = TAG_TO_STATE.get(qName);

            if (cur != null) {
                trace.remove(cur);
            }

            if (cur == State.TEXT && isInterestPage) {
                try {
                    wikiPageListener.processPage(pageId, pageTitle.toString(), pageText);
                } catch (Exception e) {
                    throw new SAXException(e);
                }
            }
        }

        @Override
        public void characters(final char[] ch, final int start, final int length) throws SAXException {
            if (trace.contains(State.TITLE)) {
                pageTitle.append(ch, start, length);
            } else if (trace.contains(State.TEXT) && isInterestPage) {
                pageText.append(ch, start, length);
            } else if (trace.contains(State.ID) && !trace.contains(State.REVISION)) {
                pageId.append(ch, start, length);
            }
        }
    }
}
