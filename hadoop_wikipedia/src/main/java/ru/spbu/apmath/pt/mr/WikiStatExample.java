package ru.spbu.apmath.pt.mr;

import com.google.common.collect.Iterators;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.ClassicAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.wikipedia.WikipediaTokenizer;
import org.apache.lucene.util.Version;
import ru.spbu.apmath.pt.util.LoggerUtils;

import java.io.IOException;
import java.io.StringReader;

/**
 * Пример работы с Википедией. Считаем сколько слов оканчивается на -ly. Первый аргумент - путь до файлов с текстом,
 * второй - куда записывать результат.
 */
public class WikiStatExample extends Configured implements Tool {
    private static final Logger LOG = Logger.getLogger(WikiStatExample.class);

    public static void main(final String[] args) throws Exception {
        LoggerUtils.initLogger();

        ToolRunner.run(new Configuration(), new WikiStatExample(), args);
    }

    @Override
    public int run(final String[] args) throws Exception {
        // обрабатывать будем в три потока (имеет смысл при работе на одной машине)
        getConf().setInt("mapreduce.local.map.tasks.maximum", 3);

        final Job job = new Job(getConf());
        job.setJobName("wordcount");

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        // указываем Mapper и Reducer
        job.setMapperClass(TextMapper.class);
        job.setReducerClass(WikiReducer.class);

        job.setInputFormatClass(SequenceFileInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.setInputPaths(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        // удаляем директорию с результатом
        FileSystem.get(getConf()).delete(new Path(args[1]), true);
        job.waitForCompletion(true);

        return 0;
    }

    public static class TextMapper extends Mapper<Text, Text, Text, IntWritable> {
        private static final IntWritable ONE = new IntWritable(1);

        private final Text textBuffer = new Text();

        // компонент из Lucene, простой токенайзер.
        private final ClassicAnalyzer сlassicAnalyzer = new ClassicAnalyzer(Version.LUCENE_45);

        @Override
        protected void map(final Text key, final Text value, final Context context) throws IOException, InterruptedException {
            try {
                // в key - названии вики-статьи, в value - содержание  (обычный текст)

                // разделяем текст на отдельные слова
                final TokenStream ts = сlassicAnalyzer.tokenStream("", value.toString());
                ts.reset();

                // проходим по всем "словам"
                while(ts.incrementToken()) {
                    final CharSequence word = ts.getAttribute(CharTermAttribute.class);
                    final String str = word.toString();

                    // проверяем если слово оканчивается на -ly
                    if (str.endsWith("ly")) {
                        textBuffer.set(str);
                        context.write(textBuffer, ONE);
                    }
                }
            } catch (Exception ex) {
                LOG.error("", ex);
            }

        }
    }

    public static class WikiReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
        @Override
        protected void reduce(final Text key, final Iterable<IntWritable> values, final Context context) throws IOException, InterruptedException {
            final int size = Iterators.size(values.iterator());
            // если слово встретилось больше 10 раз - пишем его в результат
            if (size > 10) {
                context.write(key, new IntWritable(size));
            }
        }
    }

    public static class WikiMapper extends Mapper<Text, Text, Text, IntWritable> {
        private static final IntWritable wr = new IntWritable(1);

        @Override
        protected void map(final Text key, final Text value, final Context context) throws IOException, InterruptedException {
            try {
                final WikipediaTokenizer wt = new WikipediaTokenizer(new StringReader(value.toString()));
                wt.reset();
                while(wt.incrementToken()) {
                    final CharSequence word = wt.getAttribute(CharTermAttribute.class);
                    if (word != null) {
                        final String str = word.toString().toLowerCase();
                        if (str.endsWith("ly")) {
                            context.write(new Text(str), wr);
                        }
                    }
                }
            } catch (Exception ex) {
                LOG.error("", ex);
            }

        }
    }
}