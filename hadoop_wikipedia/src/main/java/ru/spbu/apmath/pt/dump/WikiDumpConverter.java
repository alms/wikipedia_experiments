package ru.spbu.apmath.pt.dump;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.spbu.apmath.pt.dump.util.IWikiPageListener;
import ru.spbu.apmath.pt.dump.util.WikiArchiveProcessor;
import ru.spbu.apmath.pt.util.LoggerUtils;

import java.io.File;
import java.io.IOException;

/**
 * Первый аргумент - путь до xml-файла c дампом Википедии, второй - директория для записи
 * результата в формате SequenceFile
 */
public class WikiDumpConverter {

    private static final Logger LOG = LoggerFactory.getLogger(WikiDumpConverter.class);

    private static final int ARTICLES_PER_FILE = 50000;

    public static void main(final String[] args) throws Exception {
        LoggerUtils.initLogger();

        new WikiArchiveProcessor(new File(args[0])).process(new Listener(args[1]));
    }


    private static class Listener implements IWikiPageListener {
        private final String outDir;

        private int numArticles = 0;
        private int numFiles = 0;
        private SequenceFile.Writer wr;

        private Listener(final String outDir) {
            this.outDir = outDir;
        }

        private SequenceFile.Writer createWriter(final String name) {
            try {
                return  SequenceFile.createWriter(new Configuration(),
                            SequenceFile.Writer.file(new Path(new Path(outDir), name)),
                            SequenceFile.Writer.keyClass(Text.class),
                            SequenceFile.Writer.valueClass(Text.class)
                );
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public void start()  {
            wr = createWriter("part-" + numFiles);
        }

        @Override
        public boolean isInterestPage(CharSequence id, CharSequence title)  {
            return true;
        }

        @Override
        public void processPage(CharSequence id, String title, CharSequence text) {
            try {
                numArticles++;
                if(numArticles % ARTICLES_PER_FILE == 0) {
                    wr.close();
                    numFiles++;
                    wr = createWriter("part-" + numFiles);
                    LOG.info("Article processed: " + numArticles);
                }
                wr.append(new Text(title), new Text(text.toString()));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public void end() {
            try {
                wr.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
