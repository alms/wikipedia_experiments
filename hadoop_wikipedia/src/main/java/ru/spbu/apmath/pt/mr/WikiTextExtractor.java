package ru.spbu.apmath.pt.mr;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;
import org.dbpedia.extraction.sources.WikiPage;
import org.dbpedia.extraction.util.Language;
import org.dbpedia.extraction.wikiparser.PageNode;
import org.dbpedia.extraction.wikiparser.WikiParser;
import org.dbpedia.extraction.wikiparser.WikiTitle;
import org.dbpedia.extraction.wikiparser.impl.simple.SimpleWikiParser;
import ru.spbu.apmath.pt.util.LoggerUtils;

import java.io.IOException;

/**
 * Конвертируем файлы в формате SequenceFile из wikitext в обычный текст. Первый аргумент - путь до файлов с wikitext,
 * второй - куда записывать результат.
 */
public class WikiTextExtractor extends Configured implements Tool {
    private static final Logger LOG = Logger.getLogger(WikiTextExtractor.class);

    public static void main(final String[] args) throws Exception {
        LoggerUtils.initLogger();

        ToolRunner.run(new Configuration(), new WikiTextExtractor(), args);
    }

    @Override
    public int run(final String[] args) throws Exception {
        // мы хотим сжимать результат
        getConf().setBoolean("mapred.compress.map.output", true);
        // обрабатывать будем в три потока (имеет смысл при работе на одной машине)
        getConf().setInt("mapreduce.local.map.tasks.maximum", 3);

        final Job job = new Job(getConf());
        job.setJobName("wikitext parser");

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        // указываем Mapper. Reducer'a нет
        job.setMapperClass(WikiMapper.class);

        // указываем форматы входного(ых) и выходного(ых) файлов
        job.setInputFormatClass(SequenceFileInputFormat.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);

        // указываем где искать эти файлы
        FileInputFormat.setInputPaths(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        FileOutputFormat.setCompressOutput(job, true);

        // на всякий случай очищаем директорию для записи результата
        FileSystem.get(getConf()).delete(new Path(args[1]), true);
        job.waitForCompletion(true);

        return 0;
    }

    public static class WikiMapper extends Mapper<Text, Text, Text, Text> {
        // создаем парсер
        private final WikiParser parser = new SimpleWikiParser();

        private final Text text = new Text();

        @Override
        protected void map(final Text key, final Text value, final Context context) throws IOException, InterruptedException {
            try {
                // в key - названии вики-статьи, в value - содержание  (викитекст)
                final PageNode node = parser.apply(new WikiPage(WikiTitle.parse(key.toString(), Language.English()), value.toString()));

                // проверяем, что это не редирект на другую статью
                if (!node.isRedirect()) {
                    // извлекаем текст
                    final String pureText = node.toPlainText();
                    // проверяем, что статья достаточно длинная
                    if (pureText.length() > 50) {
                        text.set(pureText);
                        context.write(key, text);
                    }
                }
            } catch (Exception ex) {
                LOG.error("Failed: " + key.toString());
            }
        }
    }

}